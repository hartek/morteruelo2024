from config import create_app #-Line 1
from celery import shared_task 
from time import sleep
import requests
import json
from celery.utils.log import get_task_logger
from time import sleep

flask_app = create_app()  #-Line 2
celery_app = flask_app.extensions["celery"] #-Line 3

logger = get_task_logger(__name__)

@shared_task(ignore_result=False)
def vt_getresult(hash_to_check, response_url) -> dict: # This will query the VT API for a hash
    api_key = "<>" # This is a bad idea
    headers = {
      "Accept-Encoding": "gzip, deflate",
      "x-apikey": api_key
    }
    #sleep(10)
    response = requests.get(f'https://www.virustotal.com/api/v3/files/{hash_to_check}', headers=headers)
    vt_json_response = response.json()

    if "data" not in vt_json_response:
        response = requests.post(response_url,
            headers={"content-type": "application/json"},
            json={"text": "Data not found!", "response_type": "in_channel"})
        return vt_json_response

    response_dict = {
        "meaningful_name" : vt_json_response["data"]["attributes"]["meaningful_name"],
        "last_submission_date" : vt_json_response["data"]["attributes"]["last_submission_date"],
        "sha1" : vt_json_response["data"]["attributes"]["sha1"],
        "md5" : vt_json_response["data"]["attributes"]["md5"],
        "sha256" : vt_json_response["data"]["attributes"]["sha256"],
        "last_analysis_stats" : vt_json_response["data"]["attributes"]["last_analysis_stats"],
    }
    response_str = "\n".join([f"{key}: {value}" for key,value in response_dict.items()])

    response = requests.post(response_url,
        headers={"content-type": "application/json"},
        json={"text": response_str, "response_type": "in_channel"})
    logger.info(str(response), response.text)

    return vt_json_response
