from tasks import flask_app, vt_getresult #-Line 1
from celery.result import AsyncResult#-Line 2
from flask import request,jsonify,abort
import requests
import time
import hmac
import hashlib

@flask_app.post("/vthash")
def vthash():
    try: 
        verification = verify_request(request)
        if not verification: 
            abort(401)
        hash_to_check = request.form["text"]  
        response_url = request.form["response_url"]
        json_response = vt_getresult.delay(hash_to_check, response_url) # Function that queries VT
    except KeyError:
        return {"text": "Incorrect command"}
    return {"text": "VT query queued!", "response_type": "in_channel"}

def verify_request(request): 
    slack_signing_secret = 'e8b2fb6d0dc2ad981f6fa31bb60fdbb8'
    timestamp = request.headers['X-Slack-Request-Timestamp']
    if abs(time.time() - int(timestamp)) > 60 * 5:
        # The request timestamp is more than five minutes from local time.
        # It could be a replay attack, so let's ignore it.
        print("Signature expired!")
        return False
    sig_basestring = 'v0:' + timestamp + ':' + request.get_data().decode('utf-8')
    print(request)
    print(sig_basestring)
    req_signature = 'v0=' + hmac.new(
        slack_signing_secret.encode('utf-8'),
        sig_basestring.encode('utf-8'),
        hashlib.sha256,
    ).hexdigest()
    slack_signature = request.headers['X-Slack-Signature']
    if len(req_signature) != len(slack_signature) or \
            not hmac.compare_digest(req_signature, slack_signature):
        print("Incorrect signature!")
        print(req_signature)
        print(slack_signature)
        return False
    return True


if __name__ == "__main__":
    flask_app.run(debug=True, port=8000)
