#!/usr/bin/env python3
"""
License: MIT License
Copyright (c) 2023 Miel Donkers

Very simple HTTP server in python for logging requests
Usage::
    ./server.py [<port>]
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib import parse
import logging
import requests
import json
import threading

class S(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_POST(self):
        logging.info("POST request received")
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        post_params= parse.parse_qs(str(post_data, 'utf-8')) # Parses POST parameters
        logging.info(post_params)
        try: 
            hash_to_check = post_params['text'][0] # Get hash to check
            json_response = self.vt_getresult(hash_to_check) # Function that queries VT
        except KeyError:
            json_response = "aqui no hay na, ponme algo"
        self._set_response() # Prepare and send response
        self.wfile.write(json.dumps(json_response).encode('utf-8'))


    def vt_getresult(self, hash_to_check): # This will query the VT API for a hash
        api_key = "<>" # This is a bad idea
        headers = {
          "Accept-Encoding": "gzip, deflate",
          "x-apikey": api_key
        }
        response = requests.get(f'https://www.virustotal.com/api/v3/files/{hash_to_check}', headers=headers)
        json_response = response.json()
        return json_response


def run(server_class=HTTPServer, handler_class=S, port=8000):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
